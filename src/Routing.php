<?php
namespace Sharecoto;

class Routing
{
    /**
     * @var \Slim\Slim
     */
    protected $app;

    public function __construct(\Slim\Slim $app = null)
    {
        if (!$app) {
            $app = \Slim\Slim::getInstance();
        }
        $this->app = $app;
    }

    public function register()
    {
        return $this;
    }
}

